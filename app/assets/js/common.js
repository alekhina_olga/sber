$(function() {

    var header = $('.header'),
        logo = $('.header').find('.js-logo'),
        menu = $('#popup-menu'),
        links = $('.js-header-links'),
        dopLinks = $('.js-header-dop-links'),
        menuWrap = $('.js-header-menu-wrap'),
        local = $('.js-menu-item--local'),
        personal = $('.js-menu-item--personal'),
        searchPopup = $('#popup-search'),
        search = $('.js-search'),
        menuOverlay = $('.menu-overlay'),
        menuBtnOpen = $('.js-menu-open');

    function MenuChange() {
        links.appendTo(header);
        dopLinks.appendTo(header);
        menuWrap.appendTo(header);
        logo.prependTo(dopLinks.children('.container'));
        personal.appendTo(links.children('.container'));
        local.appendTo(links.children('.container'));
        search.appendTo(dopLinks.children('.container'));
        $('.js-item-drop').each(function(){
            $(this).next('.js-drop').appendTo(this);
        });
        menu.removeClass('open');
        menuOverlay.removeClass('open');
        menuBtnOpen.removeClass('open');
    }

    function MenuChangePrev() {
        links.appendTo(menu);
        menuWrap.appendTo(menu);
        dopLinks.appendTo(menu);
        logo.appendTo(header.children('.container'));
        local.prependTo(links.children('.container'));
        personal.prependTo(links.children('.container'));
        search.appendTo(searchPopup);
        $('.js-item-drop').each(function(){
            $(this).find('.js-drop').insertAfter(this);
        });
    }

    if ($(window).width() >= 980) {
        MenuChange();
    }

    $(window).resize(function() {
        if ($(window).width() >= 980) {
            MenuChange();
        }
        else {
            MenuChangePrev();
        }
    });

});

// Yandex Metrica
(function (d, w, c) {
    (window[c] = w[c] || []).push(function () {
        try {
            w.yaCounter49292653 = new Ya.Metrika2({
                id: 49292653,
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });
        } catch (e) {
        }
    });

    var n = document.getElementsByTagName("script")[0],
        s = document.createElement("script"),
        f = function () {
            n.parentNode.insertBefore(s, n);
        };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/tag.js";

    if (w.opera === "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else {
        f();
    }
})(document, window, "yandex_metrika_callbacks2");

// google analytics
(function () {
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120936965-1');
})();

(function () {
    if (/MSIE/i.test(navigator.userAgent)) {
        document.body.classList.add('ie');
    }
    if (/MSIE 11/i.test(navigator.userAgent)) {
        document.body.classList.add('ie11');
    }
    if (/MSIE 10/i.test(navigator.userAgent)) {
        document.body.classList.add('ie10');
    }
    if (/Edge\/\d./i.test(navigator.userAgent)){
        document.body.classList.add('edge');
    }
})();

$(function(){
    var arrow = $(".arrow-up");
    var visible = window.scrollY > 0;
    arrow[visible ? 'show' : 'hide']();

    arrow.bind('click', function(e){
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 400);    
    });


    function toggleArrow() {
        if (!visible && window.scrollY > 0) {
            arrow.fadeIn(function () {
                visible = true;
            });
        } else if (visible && window.scrollY === 0) {
            arrow.fadeOut(function () {
                visible = false;
            });
        }
    }

    $(window).on('scroll', toggleArrow);
    toggleArrow();
});
$(function() {
	$('.js-slider-banner').slick({
        dots: true,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 980,
                settings: {
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
});

$(function() {

    $(document).on('click', '.js-search-open', function(e){
        var searchPopup = $('#popup-search');
        if (searchPopup.hasClass('open')) {
            searchPopup.removeClass('open');
            $(this).removeClass('open');
            searchPopup.find('input').val('');
        }
        else {
            searchPopup.addClass('open');
            $(this).addClass('open');
        }
    });

    $(document).on('click', function (e){
        var searchPopup = $('#popup-search'),
            searchBtn = $('.js-search-open');
		if (!searchBtn.is(e.target)
		    && searchBtn.has(e.target).length === 0
            && !searchPopup.is(e.target)
		    && searchPopup.has(e.target).length === 0
            && searchPopup.hasClass('open')) {
                searchPopup.removeClass('open');
                searchPopup.removeClass('open');
                $('.js-search-open').removeClass('open');
                searchPopup.find('input').val('');
		}
	});

});

$(function() {

    $(document).on('click', '.js-menu-open', function(e){
        var menuPopup = $('#popup-menu');
        if (menuPopup.hasClass('open')) {
            menuPopup.removeClass('open');
            $(this).removeClass('open');
            $('.menu-overlay').removeClass('open');
            $('body').css('overflow', 'unset');
        }
        else {
            menuPopup.addClass('open');
            $(this).addClass('open');
            $('.menu-overlay').addClass('open');
            $('body').css('overflow', 'hidden');
        }
    });

    $(document).on('click', function (e){
        var menuPopup = $('#popup-menu'),
            menuBtn = $('.js-menu-open');

        if (!menuBtn.is(e.target) &&
            menuBtn.has(e.target).length === 0) {
            menuPopup.removeClass('open');
            $('.js-menu-open').removeClass('open');
            $('.menu-overlay').removeClass('open');
            $('body').css('overflow', 'unset');
            $('.js-drop').css('height', '0');
            $('.js-item-drop').removeClass('drop');
        }
	});

    $(document).on('click', '.js-item-drop', function(e){
        e.preventDefault();
        e.cancelBubble = true;
        e.stopPropagation && e.stopPropagation();
        var child, objHeight;

        if ($(this).closest('#popup-menu').length > 0) {
           child = $(this).next('.js-drop');
        } else {
           child = $(this).find('.js-drop');
        }

        objHeight = child.children('.object').height();

        if ($(this).is('.drop')) {
            child.css('height', '0');
            $(this).removeClass('drop');
        } else {
            $('.js-drop').css('height', '0');
            $('.js-item-drop').removeClass('drop');


            if ($(this).closest('#popup-menu').length > 0) {
                child.css('height', objHeight);
            } else {
                child.css('height', 'auto');
            }

            $(this).addClass('drop');
        }
    });

});


$(function() {

    $('.events').each(function(){
        $(this).closest('.events').find('.js-events').css('height', $(this).closest('.events').find('.js-wrap-event').outerHeight());
    });

    $(document).on('click', '.js-event-btn', function(e){

        var mHeight = $(this).closest('.events').find('ul').outerHeight(),
            dropEvent = $(this).closest('.events').find('.js-events'),
            height = $(this).closest('.events').find('.js-wrap-event').outerHeight();

        if($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).text('Показать ещё');
            dropEvent.css('height', height);
        }
        else {
            $(this).addClass('open');
            $(this).text('Скрыть');
            dropEvent.css('height', mHeight);
        }
    });

    $(window).resize(function(){
        $('.events').each(function(){
            $(this).closest('.events').find('.js-events').css('height', $(this).closest('.events').find('.js-wrap-event').outerHeight());
        });
    })

})

$(function() {

    function SearchInputCheck() {

        var label = $('.js-search-label');

        if ($('.js-search').find('input').val() == '') {
            label.removeClass('hide');
        }
        else {
            label.addClass('hide');
        }
    };

    $(document).on('focus', '.js-search', function(e) {
        SearchInputCheck();
    });

    $(document).on('focusout', '.js-search', function(e) {
        SearchInputCheck();
    });

    $(document).on('keyup', '.js-search', function(e) {
        SearchInputCheck();
    });

    $(document).on('click', '.js-search', function(e) {
        SearchInputCheck();
    });
})
