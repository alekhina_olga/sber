$(function(){
    var arrow = $(".arrow-up");
    var visible = window.scrollY > 0;
    arrow[visible ? 'show' : 'hide']();

    arrow.bind('click', function(e){
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 400);    
    });


    function toggleArrow() {
        if (!visible && window.scrollY > 0) {
            arrow.fadeIn(function () {
                visible = true;
            });
        } else if (visible && window.scrollY === 0) {
            arrow.fadeOut(function () {
                visible = false;
            });
        }
    }

    $(window).on('scroll', toggleArrow);
    toggleArrow();
});