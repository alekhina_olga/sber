$(function() {

    var header = $('.header'),
        logo = $('.header').find('.js-logo'),
        menu = $('#popup-menu'),
        links = $('.js-header-links'),
        dopLinks = $('.js-header-dop-links'),
        menuWrap = $('.js-header-menu-wrap'),
        local = $('.js-menu-item--local'),
        personal = $('.js-menu-item--personal'),
        searchPopup = $('#popup-search'),
        search = $('.js-search'),
        menuOverlay = $('.menu-overlay'),
        menuBtnOpen = $('.js-menu-open');

    function MenuChange() {
        links.appendTo(header);
        dopLinks.appendTo(header);
        menuWrap.appendTo(header);
        logo.prependTo(dopLinks.children('.container'));
        personal.appendTo(links.children('.container'));
        local.appendTo(links.children('.container'));
        search.appendTo(dopLinks.children('.container'));
        $('.js-item-drop').each(function(){
            $(this).next('.js-drop').appendTo(this);
        });
        menu.removeClass('open');
        menuOverlay.removeClass('open');
        menuBtnOpen.removeClass('open');
    }

    function MenuChangePrev() {
        links.appendTo(menu);
        menuWrap.appendTo(menu);
        dopLinks.appendTo(menu);
        logo.appendTo(header.children('.container'));
        local.prependTo(links.children('.container'));
        personal.prependTo(links.children('.container'));
        search.appendTo(searchPopup);
        $('.js-item-drop').each(function(){
            $(this).find('.js-drop').insertAfter(this);
        });
    }

    if ($(window).width() >= 980) {
        MenuChange();
    }

    $(window).resize(function() {
        if ($(window).width() >= 980) {
            MenuChange();
        }
        else {
            MenuChangePrev();
        }
    });

});

// Yandex Metrica
(function (d, w, c) {
    (window[c] = w[c] || []).push(function () {
        try {
            w.yaCounter49292653 = new Ya.Metrika2({
                id: 49292653,
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });
        } catch (e) {
        }
    });

    var n = document.getElementsByTagName("script")[0],
        s = document.createElement("script"),
        f = function () {
            n.parentNode.insertBefore(s, n);
        };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/tag.js";

    if (w.opera === "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else {
        f();
    }
})(document, window, "yandex_metrika_callbacks2");

// google analytics
(function () {
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120936965-1');
})();

(function () {
    if (/MSIE/i.test(navigator.userAgent)) {
        document.body.classList.add('ie');
    }
    if (/MSIE 11/i.test(navigator.userAgent)) {
        document.body.classList.add('ie11');
    }
    if (/MSIE 10/i.test(navigator.userAgent)) {
        document.body.classList.add('ie10');
    }
    if (/Edge\/\d./i.test(navigator.userAgent)){
        document.body.classList.add('edge');
    }
})();