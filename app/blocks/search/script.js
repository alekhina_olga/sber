$(function() {

    function SearchInputCheck() {

        var label = $('.js-search-label');

        if ($('.js-search').find('input').val() == '') {
            label.removeClass('hide');
        }
        else {
            label.addClass('hide');
        }
    };

    $(document).on('focus', '.js-search', function(e) {
        SearchInputCheck();
    });

    $(document).on('focusout', '.js-search', function(e) {
        SearchInputCheck();
    });

    $(document).on('keyup', '.js-search', function(e) {
        SearchInputCheck();
    });

    $(document).on('click', '.js-search', function(e) {
        SearchInputCheck();
    });
})
