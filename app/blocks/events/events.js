$(function() {

    $('.events').each(function(){
        $(this).closest('.events').find('.js-events').css('height', $(this).closest('.events').find('.js-wrap-event').outerHeight());
    });

    $(document).on('click', '.js-event-btn', function(e){

        var mHeight = $(this).closest('.events').find('ul').outerHeight(),
            dropEvent = $(this).closest('.events').find('.js-events'),
            height = $(this).closest('.events').find('.js-wrap-event').outerHeight();

        if($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).text('Показать ещё');
            dropEvent.css('height', height);
        }
        else {
            $(this).addClass('open');
            $(this).text('Скрыть');
            dropEvent.css('height', mHeight);
        }
    });

    $(window).resize(function(){
        $('.events').each(function(){
            $(this).closest('.events').find('.js-events').css('height', $(this).closest('.events').find('.js-wrap-event').outerHeight());
        });
    })

})
