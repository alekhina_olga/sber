$(function() {

    $(document).on('click', '.js-menu-open', function(e){
        var menuPopup = $('#popup-menu');
        if (menuPopup.hasClass('open')) {
            menuPopup.removeClass('open');
            $(this).removeClass('open');
            $('.menu-overlay').removeClass('open');
            $('body').css('overflow', 'unset');
        }
        else {
            menuPopup.addClass('open');
            $(this).addClass('open');
            $('.menu-overlay').addClass('open');
            $('body').css('overflow', 'hidden');
        }
    });

    $(document).on('click', function (e){
        var menuPopup = $('#popup-menu'),
            menuBtn = $('.js-menu-open');

        if (!menuBtn.is(e.target) &&
            menuBtn.has(e.target).length === 0) {
            menuPopup.removeClass('open');
            $('.js-menu-open').removeClass('open');
            $('.menu-overlay').removeClass('open');
            $('body').css('overflow', 'unset');
            $('.js-drop').css('height', '0');
            $('.js-item-drop').removeClass('drop');
        }
	});

    $(document).on('click', '.js-item-drop', function(e){
        e.preventDefault();
        e.cancelBubble = true;
        e.stopPropagation && e.stopPropagation();
        var child, objHeight;

        if ($(this).closest('#popup-menu').length > 0) {
           child = $(this).next('.js-drop');
        } else {
           child = $(this).find('.js-drop');
        }

        objHeight = child.children('.object').height();

        if ($(this).is('.drop')) {
            child.css('height', '0');
            $(this).removeClass('drop');
        } else {
            $('.js-drop').css('height', '0');
            $('.js-item-drop').removeClass('drop');


            if ($(this).closest('#popup-menu').length > 0) {
                child.css('height', objHeight);
            } else {
                child.css('height', 'auto');
            }

            $(this).addClass('drop');
        }
    });

});
