$(function() {

    $(document).on('click', '.js-search-open', function(e){
        var searchPopup = $('#popup-search');
        if (searchPopup.hasClass('open')) {
            searchPopup.removeClass('open');
            $(this).removeClass('open');
            searchPopup.find('input').val('');
        }
        else {
            searchPopup.addClass('open');
            $(this).addClass('open');
        }
    });

    $(document).on('click', function (e){
        var searchPopup = $('#popup-search'),
            searchBtn = $('.js-search-open');
		if (!searchBtn.is(e.target)
		    && searchBtn.has(e.target).length === 0
            && !searchPopup.is(e.target)
		    && searchPopup.has(e.target).length === 0
            && searchPopup.hasClass('open')) {
                searchPopup.removeClass('open');
                searchPopup.removeClass('open');
                $('.js-search-open').removeClass('open');
                searchPopup.find('input').val('');
		}
	});

});
